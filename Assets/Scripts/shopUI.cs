using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System.Globalization;

public class shopUI : MonoBehaviour //clase encargada de la UI de la tienda
{
    public TextMeshProUGUI coinsText, MpText;
    public GameObject BlueBlockedText, GreyBlockedText;
    public Button GreyButton, BlueButton;
   
    void Update()
    {
        coinsText.text = GameManager.Instance.coins.ToString();
        MpText.text = GameManager.Instance.metalPieces.ToString();
        if (BlueBlockedText != null && GreyBlockedText != null && GreyButton != null && BlueButton != null) 
        { 
            HandleBlockTexts(); 
        }
    }

    void HandleBlockTexts() //control de la ui segun las naves estan desbloqueadas o no
    {
        if (GameManager.Instance.Sc2Unlocked)
        { 
            GreyBlockedText.SetActive(false);
            GreyButton.interactable = true;
        }
        if (GameManager.Instance.Sc3Unlocked) 
        { 
            BlueBlockedText.SetActive(false); 
            BlueButton.interactable = true;
        }
    }
    public void LoadScene(string name)
    {
        SceneManager.LoadSceneAsync(name);
    }

    public void SetSelector(int number)
    {
        GameManager.Instance.ScSelector = number; //ni me acuerdo de que co�o es esto, pero tiene que ver con el desbloqueo de las naves
    }

    public void quitGame()
    {
        Application.Quit();
    }

    public void AddHC() //esto hay que quitarlo
    {
        GameManager.Instance.metalPieces++;
    }

    public void ShowPanel(GameObject panelShow)
    {
        panelShow.SetActive(true);
        
    }
    public void HidePanel(GameObject panelHide)
    {
        panelHide.SetActive(false);

    }
}
