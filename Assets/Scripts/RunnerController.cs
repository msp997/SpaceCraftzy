using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerController : MonoBehaviour, IDamageable
{
    [HideInInspector] public SO_Enemy data;
    private float health, damage, speed;
    [HideInInspector] public Transform carroPos;
    [SerializeField] private GameObject FX_Explosion;
    

    private void Start()
    {
        SetStats(); //se ponen los stats del scriptable y se les aplica el escalado en caso de que no sea la primera vuelta de la partida
        Destroy(gameObject,12);//destruyo el objeto a los 12 segundos de spawnear, por si alguno se queda pilluco en el aire
    }
    private void Update()
    {
        ChasePlayer(); //se mueve hacia el jugador
        transform.LookAt(carroPos); //miran hacia el carro
    }

    private void SetStats() //pilla y setea las stats del scriptable, ademas de aplicar el escalado
    {           
            damage = data.damage + 4 * (PlayerController.instance.lap - 1); //a partir de la primera vuelta el da�o y la vida empiezan a escalar
            speed = data.speed;
            health = data.health + 20 * (PlayerController.instance.lap - 1);
            //Debug.Log("vida: " + health + " da�o: " + damage);       
    }
    
    private void ChasePlayer() //funcion encargada de moverse hacia el jugador
    {
        transform.position = Vector3.MoveTowards(transform.position, PlayerController.instance.gameObject.transform.position, speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        IDamageable damageable = other.GetComponent<IDamageable>();
        if (other.tag == "Player" || other.tag == "PlayerTrigger") //colision con player
        {
            damageable.TakeDamage(damage);
            GameObject explosion = Instantiate(FX_Explosion, transform.position, transform.rotation); //si se estampa muere, asi que salen las particulas de explosion
            Destroy(gameObject);             //basicamente si se estampa contra el player (el unico con damageable ademas del runner) le hace da�o y se muere
        }
       
        else
        {
            return; //colisiona con cualquier cosa que no sea player = no hace nada
        }
            
    }

     public void TakeDamage(float damage) //recibe da�o y si vida <= 0 se mata
     {
        health -= damage;
       // Debug.Log("me haces pupa! " + health);
        Death();
     }
    private void Death()
    {
        if (health <= 0) //vida menos de 0 = muere y suma score
        {
            GameManager.Instance.AddScore(100);
            Destroy(gameObject);// Debug.Log("me he matao paco");
            GameObject explosion = Instantiate(FX_Explosion, transform.position, transform.rotation);
        }
    }
}
