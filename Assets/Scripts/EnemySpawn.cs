using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public SO_Enemy EnemyData; //datos del scriptable
    [SerializeField] private Transform[] SpawnPoints; //puntos de spawn de los enemigos
    [SerializeField] private Transform carro;                         
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerTrigger"))
        {           
            SpawnEnemies();
        }
    }

    private void SpawnEnemies()
    {
        int AmountOfEnemies = Random.Range(1, SpawnPoints.Length); //decido la cantidad de enemigos que voy a spawnear (m�nimo 1)
        
        for (int i = 0; i < AmountOfEnemies; i++)
        {
            GameObject enemigo = Instantiate(EnemyData.model, SpawnPoints[i].transform.position, SpawnPoints[i].rotation); //generacion de los enemigos en los spawn points
            enemigo.GetComponent<RunnerController>().data = EnemyData; //se les pasan los datos del scriptable
            enemigo.GetComponent<RunnerController>().carroPos = carro; //se les pasa el transform del carro para que puedan mirar hacia el
        }       
    }
}
