using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Bullet : MonoBehaviour
{
    [SerializeField]  private float speed;
    [HideInInspector] public float damage;
    [SerializeField] private GameObject BulletHit;
    private void Start()
    {
        Destroy(gameObject, 3); // se destruye a la bala a los 3 segundos si no ha colisionado con nada
       
    }
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "PlayerTrigger" || other.tag == "PlayerBullet")  // colision con el player que genera la bala 
        {
            return; //�apa: si no hago esto la bala se destruye al spawnear (pendiente de corregir)
        }      
           
        

        IDamageable damageable = other.GetComponent<IDamageable>(); //comprueba que haya damageable en el otro objeto

        if (damageable == null) //no hay damageable
        {
            GameObject FX_BulletHit = Instantiate(BulletHit, transform.position,transform.rotation); //se genera la part�cula del bullet hit
            Destroy(gameObject); //se destruye
        }
       
        
        else // hay damageable
        {
            GameObject FX_BulletHit = Instantiate(BulletHit, transform.position, transform.rotation); //se genera la part�cula del bullet hit
            damageable.TakeDamage(damage);
            Destroy(gameObject);           //la bala hace da�o al objeto con el que ha colisionado si tiene damageable y se destruye
        }
    }
}
