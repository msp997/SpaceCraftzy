using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthKit : MonoBehaviour
{
    [SerializeField] private int HealPower= 10;
    [SerializeField] private float rotateSpeed = 10f, frequency = 1f, amplitude = 0.5f;

    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    void Start()
    {
        // posicion y rotacion inicial del objeto
        posOffset = transform.position;
    }

    void Update() //la caja flota y rota
    {
        transform.Rotate(0, rotateSpeed * Time.deltaTime, 0, 0); // rotar objeto en eje Y
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
        transform.position = tempPos;                                                //hacer que el objeto flote en el eje vertical
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.CompareTag("Player"))
        {
            PlayerController.instance.Heal(HealPower); // si colisiona con el jugador le cura
            Destroy(gameObject);
        }
    }
}
