using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUIController : MonoBehaviour //clase encargada de gestionar la UI del gameplay
{
    [SerializeField] private GameObject PauseInterface;
   
    #region UI FUNCTIONS
    public void PauseGame()
    {
        Time.timeScale = 0;
        PauseInterface.SetActive(true);
    }

    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    public void resumeGame()
    {
        Time.timeScale = 1;
        PauseInterface.SetActive(false);
    }

    

    public void HideElement(GameObject element)
    {
        element.SetActive(false);
    }
    public void ShowElement(GameObject element)
    {
        element.SetActive(true);
    }

    public void UnfreezeTime()
    {
        Time.timeScale = 1;
    }
    #endregion
}
