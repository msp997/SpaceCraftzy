using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Rendering.Universal;
using UnityEngine.ProBuilder.MeshOperations;

public class StarCraftPanel : MonoBehaviour
{
    public SO_Player ShipData;

   
    public Image HealthBar, DamageBar, SpeedBar, CadenceBar;
    public TextMeshProUGUI ShipNameText,scPriceText, hcPriceText;
    public GameObject BuyInterface;
    void Start()
    {     
        SetUiElements();
    }

    private void SetUiElements() //recoge los datos del scriptable y los setea en la UI
    {
        // TEXTOS
        ShipNameText.text = ShipData.s_name;
        scPriceText.text = ShipData.price_SC.ToString();
        hcPriceText.text = ShipData.price_HC.ToString();

        //BARRAS
        HealthBar.fillAmount = ShipData.health / 200; //estas divisiones son hardcode para que las barras funcionasen como queria, pero habria que hacer algo con ellas
        DamageBar.fillAmount =  ShipData.damage / 50;
        SpeedBar.fillAmount =  ShipData.speed / 6;
        CadenceBar.fillAmount = 1 - ShipData.TimeBetweenShots;
    }
    public void BuyButtonPress()
    {
        BuyInterface.SetActive(true);
    }

    public void BuyStarCraftwithSC(int ScN) //compra de la nave con moneda del juego
    {
        if (GameManager.Instance.coins >= ShipData.price_SC && ScN == 2)
        {
            GameManager.Instance.coins -= ShipData.price_SC;
            GameManager.Instance.Sc2Unlocked = true;
        }
        else if(GameManager.Instance.coins >= ShipData.price_SC && ScN == 3)
        {
            GameManager.Instance.coins -= ShipData.price_SC;
            GameManager.Instance.Sc3Unlocked = true;
        }
            
        else
            return;

        BuyInterface.SetActive(false);
    }
   
    public void CloseBuyInterface()
    {
        BuyInterface.SetActive(false);
    }
    public void OpenBuyInterface()
    {
        BuyInterface.SetActive(true);
    }
}
