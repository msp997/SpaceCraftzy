using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public static HealthBar instance;
    [SerializeField] private Image healthBar;
    [HideInInspector] public float Barhealth, BarmaxHealth;
    [SerializeField] private TextMeshProUGUI scoreText;
 
    float lerpSpeed;
    private void Start()
    {
        instance = this; //singleton
        lerpSpeed = Time.deltaTime * 3;
    }

    void Update()
    {
        updateScoreText();
        if (healthBar != null)
        {
            HealthBarFiller();
            colorChanger();
        }
        else
            return;
       
    }

    void HealthBarFiller() //esto hace que la barra se modifique haciendo una especie de animacion
    {
        healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, Barhealth / BarmaxHealth, lerpSpeed);
    }

    void colorChanger() //la barra cambia de color segun el hp del player
    {
        Color healthColor = Color.Lerp(Color.red, Color.green, (Barhealth / BarmaxHealth));
        healthBar.color = healthColor;
    }

    private void updateScoreText() //updatea las coins
    {
        scoreText.text =  GameManager.Instance.coins.ToString();
    }

}
