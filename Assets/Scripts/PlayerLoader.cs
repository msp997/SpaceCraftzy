using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class PlayerLoader : MonoBehaviour //esta clase es una intermediaria entre el scriptable y el prefab de la nave que tiene el player controller
{
    
    public   GameObject carro;

    public   SO_Player[] playerData;
    
    void Start()
    {      
  
            LoadPlayerData(GameManager.Instance.ScSelector); //carga el scriptable que le indique el game manager
  
    }

    
    void Update()
    {
        if (playerData == null) { return; } //esto ni me acuerdo de por que lo hice XDDDDDDDDDDDDDDDDDDDDD
    }

   

    private void LoadPlayerData( int selector)
    {
        SO_Player data= playerData[selector]; //del array de los SO del player, se carga el que indique el game manager
        GameObject modelo = Instantiate(data.model); //se carga el prefab de la nave del scriptable
        modelo.transform.SetParent(transform, false); //la nave se emparenta con el player loader
        modelo.transform.rotation = carro.transform.rotation; //set de la rotacion
        modelo.transform.localPosition = Vector3.forward * 6;       //la nave se mueve hacia alante para no estorbar el pov del player
    }

   
}
