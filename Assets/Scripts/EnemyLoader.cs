using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLoader : MonoBehaviour
{
    public SO_Enemy EnemyData;
    
    public void LoadEnemy()
    {
        GameObject enemigo = Instantiate(EnemyData.model, transform.position, transform.rotation);
    }
}
