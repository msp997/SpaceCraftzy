using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [HideInInspector] public bool Sc2Unlocked, Sc3Unlocked; //bools que indican si las naves de la tienda estan desbloqueadas o no
    public static GameManager Instance;
    [HideInInspector] public float coins = 0;
    [HideInInspector] public int ScSelector;
    private void Awake()
    {
       
        Sc2Unlocked = false; 
        Sc3Unlocked = false;

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
       
        Application.targetFrameRate = 60; //max fps
        DontDestroyOnLoad(this);
    }
    #region UI FUNCTIONS
    

    public void CloseGame()
    {
        Application.Quit();
    }

    public void LoadScene(string name)
    {
        SceneManager.LoadSceneAsync(name);
    }

    
    #endregion
   
    public void AddScore(int scoreAmount) //a�ade score al player (soft currency)
    {
        coins += scoreAmount;
    }
    
   
}
