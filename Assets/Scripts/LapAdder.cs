using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapAdder : MonoBehaviour
{
    [SerializeField] private GameObject Health_Kit;
    [SerializeField] private Transform HK_SpawnPoint;
    [SerializeField] private GameObject Scene_HK;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerTrigger"))
        {
            PlayerController.instance.lap++; //suma 1 vuelta
            if (Scene_HK == null)
            {
                GameObject HK = Instantiate(Health_Kit); //spawnea el med kit si no lo encuentra 
              //  Debug.Log("spawned med kit");
                Scene_HK = HK; // vuelve a hacer la ref
            }
            else
            {
                //Debug.Log("med kit already spwaned"); 
                return;
            }
            
            //Debug.Log("lap added");
        }
    }
}
