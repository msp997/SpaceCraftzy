using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, IDamageable
{
    PlayerLoader loader; //de aqui saca los stats del scriptable

    private float TimeBetweenShots, shotRateTime, speed, damage, inv_timer = 2;
    [HideInInspector] public float health, maxHealth;
    [SerializeField] private bool Mobile_Controls = false;

    [HideInInspector] public int lap;
    [HideInInspector] public bool PlayerDiedOnce = false, playerInvincible = false;
    [SerializeField] GameObject balas, FX_Explosion, FX_Heal;
    
    [SerializeField] Transform[] spawnPoints;
    [HideInInspector] public static PlayerController instance; //singleton

    
    
    void Start()
    {
        loader = GetComponentInParent<PlayerLoader>();
        GetScriptableObjectData();
        instance = this; //singleton
        Time.timeScale = 1;
        lap = 1;
        
        //Debug.Log(health);
    }

  
    void Update()
    {
        Movement();
        Shoot();       
        //if (Input.GetKeyDown(KeyCode.K)) Debug.Log("mi vida es " + health);
        if (playerInvincible)
        {
            inv_timer -= Time.deltaTime;
            if (inv_timer <= 0) playerInvincible = false; //control de la invencibilidad del jugador
        }
        
    }

    

    void Movement()
    {  
        transform.localPosition += new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0) * speed * Time.deltaTime;
        ClampPosition(); //impide que la nave se salga de pantalla
    }

    void ClampPosition()
    {
        float x = Math.Clamp(transform.localPosition.x, -4, 4);
        float y = Math.Clamp(transform.localPosition.y, -2.8f, 2.8f);
        transform.localPosition = new Vector3(x, y, 6);
    }


    private void GetScriptableObjectData() //fija las stats del scriptable
    {
        speed                           = loader.playerData[GameManager.Instance.ScSelector].speed; 
        TimeBetweenShots                = loader.playerData[GameManager.Instance.ScSelector].TimeBetweenShots;    
        damage                          = loader.playerData[GameManager.Instance.ScSelector].damage;
        maxHealth                       = loader.playerData[GameManager.Instance.ScSelector].health;
        health                          = loader.playerData[GameManager.Instance.ScSelector].health;
        HealthBar.instance.Barhealth    = health;                   //se setean las variables de la barra de vida de la UI, esto hay que moverlo a otro script
        HealthBar.instance.BarmaxHealth = maxHealth;
    }
    
    
    public void Shoot()
    {
        
            if (Input.GetMouseButton(0) && Time.time > shotRateTime)
            {
                for (int i = 0; i < spawnPoints.Length; i++) //genera 2 balas en los ca�ones de la nave y las asigna el da�o del scriptable, estaria guay tambien modificar su
                                                             //velocidad a trav�s de los scriptables
                {
                    GameObject newBullet;
                    newBullet = Instantiate(balas, spawnPoints[i].position, spawnPoints[i].rotation);
                    newBullet.GetComponent<Bullet>().damage = damage;
                }
                shotRateTime = Time.time + TimeBetweenShots;
            }
        
        
    }

    public void TakeDamage(float damage) //funcion del idamageable
    {
        if (!playerInvincible)
        {
            health -= damage; // se resta salud
            HealthBar.instance.Barhealth = health; //modificacion de barra de vida
            Debug.Log("mi vida tras el golpe es " + health);
        }
        else return;
       
        
        if (health <= 0 && !PlayerDiedOnce)
        {
            health = 0;
           
        }
        else if (health<= 0 && PlayerDiedOnce)
        {
            Die();
        }
    }

    public void Heal(int HealAmount)
    {
        health += HealAmount; //se a�ade salud
        HealthBar.instance.Barhealth = health; //modificacion de barra de vida
        GameObject healFX = Instantiate(FX_Heal, transform.position, transform.rotation); // particulas heal
        healFX.transform.SetParent(transform); // lo emparento para que las particulas sigan a la nave
        if (health > maxHealth) //la salud no puede ser mayor que la salud maxima
        {
            health = maxHealth;
        }
        //Debug.Log("mi vida es " + health);
    }

    void Die() //funcion pa morir y tal
    {
        GameManager.Instance.LoadScene("Menu");
        //Time.timeScale = 1f;
        GameObject deathFX = Instantiate(FX_Explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }  
}
