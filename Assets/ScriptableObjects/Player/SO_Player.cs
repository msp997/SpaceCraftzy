using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Scriptables", menuName ="My scriptables/PlayerScriptable")]
public  class SO_Player : ScriptableObject
{
    public GameObject model;
    public string s_name;
    public float speed, damage, health,TimeBetweenShots, price_SC, price_HC;
}
