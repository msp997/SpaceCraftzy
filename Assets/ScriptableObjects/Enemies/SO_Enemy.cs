using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Scriptables", menuName ="My scriptables/EnemyScriptable")]
public class SO_Enemy : ScriptableObject
{
    public GameObject model;
   
    public float TimeBetweenShots, health, damage, speed;
}
